# -*- coding: utf-8 -*-
{
    'name': "Mi Tienda",  # Module title
    'summary': "Añade productos a tu propia tienda",  # Module subtitle phrase
    'description': """Long description""",  # You can also rst format
    'author': "Parth Gajjar",
    'website': "http://www.example.com",
    'category': 'Uncategorized',
    'version': '12.0.1',
    'depends': ['base'],
    # This data files will be loaded at the installation (commented becaues file is not added in this example)
    'data': [
        'security/groups.xml',
        'security/ir.model.access.csv',
        'views/store_product.xml',
        'views/store_order.xml',
        'views/store_product_categ.xml'
    ],
    # This demo data files will be loaded if db initialize with demo data (commented becaues file is not added in this example)
    # 'demo': [
    #     'demo.xml'
    # ],
}