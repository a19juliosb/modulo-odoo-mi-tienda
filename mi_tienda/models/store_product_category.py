from odoo import models, fields, api
from odoo.exceptions import ValidationError


class ProductCategory(models.Model):
    _name="store.product.category"

    _parent_store = True
    _parent_name = "parent_id"

    name = fields.Char("Categoria")
    description = fields.Text("Descripcion")

    parent_id = fields.Many2one(
        'store.product.category',
        string='Categoria Padre',
        ondelete='restrict',
        index=True
    )
    child_ids = fields.One2many(
        'store.product.category', 'parent_id',
        string='Categorias Hijas')
    parent_path = fields.Char(index=True)

    @api.constrains('parent_id')
    def _check_hierarchy(self):
        if not self._check_recursion():
            raise models.ValidationError('No se pueden crear categorias recursivas')