# -*- coding: utf-8 -*-
import logging

from odoo import models, fields, api
from odoo.exceptions import UserError
from odoo.tools.translate import _

from datetime import datetime, timedelta


logger = logging.getLogger(__name__)

class StoreProduct(models.Model):
    _name="store.product"
    _description="Productos Tienda"

    name=fields.Char("Nombre Producto",required=True)
    before_prize=fields.Float("Precio antes IVA", required=True)
    iva=fields.Selection([("0.21","21%"),("0.1","10%"),("0.04","4%")],"IVA",default="0.21")
    after_prize=fields.Float("Precio Final(€)",compute="total_prize")
    stock=fields.Integer("Stock Actual",default=0)
    product_image= fields.Binary("Imagen")

    provider_id= fields.Many2many("res.partner", string="Proveedores")
    category_id= fields.Many2one("store.product.category", string="Categoria")

    order_ids= fields.One2many("store.order", inverse_name="product_id", string="Productos")

    @api.multi
    def total_prize(self):
        for prize in self:
            prize.after_prize=prize.before_prize*(1+float(prize.iva))


class StoreOrder(models.Model):
    _name="store.order"
    _description="Pedidos de la Tienda"
    _rec_name="product_id"

    product_id=fields.Many2one("store.product",required=True)
    date = fields.Date("Fecha Realizado")
    quantity=fields.Integer("Cantidad")
    prize_order=fields.Float("Precio Total(€)",compute="calculate_prize")

    @api.multi
    def calculate_prize(self):
        for prize in self:
            prize.prize_order=prize.product_id.after_prize*prize.quantity

    @api.constrains('quantity')
    def _check_stock(self):
        for stock in self:
            if(stock.quantity>stock.product_id.stock):
                raise models.ValidationError("No hay stock para realizar el pedido")
            else:
                stock.product_id.stock=stock.product_id.stock-stock.quantity
