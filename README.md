## Modulo de Odoo - Mi Tienda

El módulo desarrollado muestra de manera breve el catálogo de los productos de una tienda y un registro de pedidos para cada producto.

Esto se hace a través de las 3 vistas del módulo: Productos, Pedidos y Categorías.

La vista principal es la de Productos, que usa kanban para mostrar los productas de manera gráfica.

![Vista Productos Kanban](images/vkproductos.PNG)

Al seleccionar un producto muestra un formulario con todos sus campos

![Vista Productos Formulario](images/vfproductos.PNG)

Al crear un pedido para el producto muestra un formulario

![Vista Pedidos Formulario](images/vfpedidos.PNG)

Si el stock actual de un producto es menor que la cantidad del pedido se muestra un aviso

![Vista Pedidos Error](images/vfepedidos.PNG)

Si no, se deduce la cantidad del pedido del stock actual

![Vista Pedidos Formulario Exito](images/vfpedidose.PNG)

La vista de Pedidos

![Vista Pedidos Arbol](images/vtpedidos.PNG)

La vista de Categorías

![Vista Categorias](images/vtcategorias.PNG)

Al seleccionar una categoría se muestra un formulario

![Vista Categorias Formulario](images/vfcategorias.PNG)

Si se trata de añadir una categoría recursiva se muestra un aviso

![Vista Categorias Error](images/vfecategorias.PNG)

Este módulo contiene las siguientes clases:

### StoreProduct

Campos de la clase:

* **name**: de tipo Char, guarda el nombre de los Productos.
* **product_image**: de tipo Binary, guarda la imagen de los Productos.
* **before_prize**: de tipo Float, guarda el precio de los productos antes de aplicarles el IVA.
* **iva**: de tipo Selection, puede tomar los valores 21%, 10% y 4%, siendo 21% el valor por defecto.
* **after_prize**: de tipo Float, se trata de un campo calculado en la función *total_prize*.
* **stock**: de tipo Integer, guarda las existencias de cada producto.
* **provider_id**: de tipo Many2many, permite añadir los proveedores de los productos. En este caso, los valores provienen del modelo *res.partner*.
* **category_id**: de tipo Many2one, guarda la categoria de los productos.
* **order_ids**: de tipo One2many, guarda los pedidos para cada producto.

Funciones de la clase:

* **total_prize**: calcula el precio total sumandole el IVA al precio antes de impuestos

### StoreOrder

Campos de la clase:

* **product_id**: de tipo Many2one, guarda los productos para los que se hace un pedido.
* **date**: de tipo Date, registra la fecha en la que se realiza un pedido.
* **quantity**: de tipo Integer, guarda la cantidad de cada producto en los pedidos.
* **prize_order**: de tipo Float, se trata de un campo calculado en la función *calculate_prize*.

Funciones de la clase: 

* **calculate_prize**: calcula el precio del pedido en función del precio del producto y la cantidad.
* **_check_stock**: comprueba que exista suficiente stock para realizar un pedido, si no se diese el caso lanzaría un aviso. En caso contrario actualiza el stock del producto.

### ProductCategory

Campos de la clase:

* **name**: de tipo Char, guarda el nombre de las categorías.
* **description**: de tipo Text, guarda la descripción de las categorías.
* **parent_id**: de tipo Many2one, guarda la categoría padre.
* **child_ids**: de tipo One2many, guarda las categorías hijas.

Funciones de las clase:

* **_check_hierarchy**: lanza un aviso en caso de que se intenten crear categorías recursivas.
